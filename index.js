import express from 'express';
import fetch from 'isomorphic-fetch';
import Promise from 'bluebird';
import bodyParser from 'body-parser';
import _ from 'lodash';

const __DEV__ = true;

const app = express();
app.use(bodyParser.json());

let pc = {};

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/task3A/volumes', (req, res, next) => {
  const disks = pc.hdd;
  let obj = {};
  for (let i = 0; i < disks.length; i++) {
    if (obj[disks[i].volume] === undefined) obj[disks[i].volume] = disks[i].size
    else obj[disks[i].volume] = obj[disks[i].volume] + disks[i].size;
  }

  for (let key in obj) {
    obj[key] = obj[key]+"B";
  }

  res.status(200).json(obj);
  return;
});

app.get('/task3A/*', (req, res, next) => {
  const data = req.path;

  console.log('Requested: ' + data);
  const names = data.split('/');
  names.splice(0, 2);

  if (names[0] === 'volumes') next();

  let name = pc;
  for (let i = 0; i < names.length; i++) {
    if (names[i] !== '') {
      if (i === 0)
        name = pc[names[i]];
      else {
        if (name !== undefined) name = name[names[i]];
        else {
          res.status(404).send('Not found');
          console.log('Not found');          
          return;
        }
      }
    }
  }

  if (name === undefined) {
    res.status(404).send('Not found');
    console.log('Not found');
  } else {
    res.status(200).json(name);
    console.log('Found: ' + name);
  }

  return;
});

app.listen(3000, () => {
  const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
  fetch(pcUrl)
  .then(async (res) => {
    pc = await res.json();
    console.log('Скачали такую структуру: \n', pc);
  })
  .catch(err => {
    console.log('Чтото пошло не так:', err);
  });
  console.log('Example app listening on port 3000!');
});
